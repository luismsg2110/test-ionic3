# Desarrollo
Esta aplicación se desarrolló empleando el framework Ionic 3 (ver archivo package.json) y angular 5.
Además se emplearon los siguientes componentes:

-- Para el routing de la aplicación se empleo el componente ionic-angular v3.9.2 (NavController).


-- @angular/common/http": "^5.2.10" ==>> para hacer las peticiones HTTP a través de promesas como se ve a continuación:

```
let promise = new Promise ((resolve, reject) =>{
      this.http.put(`https://dev.tuten.cl:443/TutenREST/rest/user/${ email }`, this.body, httpOptions)
      .subscribe( data =>{
        this.info = data;
        this.errorBool = false;
        resolve();
      },
      err => {
        this.errores = err;
        this.errorMsg = err.error;
        this.errorBool = true;
        resolve();
      });
    });
    return promise;
  }
```

Usando la bandera errorBoll pra saber si la peticion retorna una respuesta correcta o erronea. En caso de ser correcta, la variable info almacena la respuesta de la api.

-- "@angular/forms": "^5.2.0"

Para validar y capturar los valores del formulario (front html) para insertarlos en la petición. Ejemplo de validación:

```
this.formLogin = this.fb.group({
      email : ['', Validators.email],
      password : ['', Validators.required]
    });
```	
	
-- Se crearon los servicios RestloginService (para realizar el login) y BookingService (para listar los booking del usuario).

--Para el filtrado del bookingId se realizó un filtrado personalizado, como se muestra a continuación:

```
@Pipe({
  name : 'filter',
})
export class FilterPipe implements PipeTransform {
  public transform(value, key: string, term: string) {
    return value.filter((item) => {
      if (item.hasOwnProperty(key)) {
        if (term) {
          let regExp = new RegExp('\\b' + term, 'gi');
          return regExp.test(item[key]);
        } else {
          return true;
        }
      } else {
        return false;
      }
    });
  }
}
```

La lista de las elementos fué realizado en el arhivo html, de la siguiente manera:

```
<ion-content padding>
  <ion-searchbar [(ngModel)]="searchString"></ion-searchbar>
  <ion-item-group *ngFor="let d of _bs.book | search: 'bookingId' :searchString">
    <ion-item-divider color="light">ID: {{d.bookingId}}</ion-item-divider>
    <ion-item>{{d.bookingId}}</ion-item>
    <ion-item>Miguel Tuten</ion-item>
    <ion-item>{{d.bookingTime}}</ion-item>
    <ion-item>{{d.locationId.streetAddress}}</ion-item>
    <ion-item>{{d.bookingPrice}}</ion-item>
  </ion-item-group>
</ion-content>
```
