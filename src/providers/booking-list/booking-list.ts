import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the BookingListProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BookingListProvider {
  book:any; 
  errorBool:boolean = false;
  head:any = {};

  constructor(public http: HttpClient) {
  }
  showBook(adminemail:string, token:string){
    this.head = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json', 'app':'APP_BCK',  'adminemail':`${adminemail}`, 'token':`${token}` })
    };
    let promise = new Promise ((resolve, reject) =>{
      this.http.get(`https://dev.tuten.cl:443/TutenREST/rest/user/miguel@tuten.cl/bookings?current=true`, this.head)
      .subscribe( data =>{
        this.book = data;
        this.errorBool = false;
        resolve();
      },
      err => {
        this.errorBool = true;
        resolve();
      });
    });
    return promise;
  }

}
