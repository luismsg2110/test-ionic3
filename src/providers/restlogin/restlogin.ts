import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the RestloginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'app':'APP_BCK',  'password':'1234' })
};

@Injectable()
export class RestloginProvider {
  public url:string;
  info:any;
  errorMsg:any;
  errores:any[]=[];
  errorBool:boolean = false;
  body:any = {};

  constructor(public http: HttpClient) {
  }

  doLogin(email:string){
    //this.http.get(`https://dev.tuten.cl:443/TutenREST/rest/user/${ email }`, httpOptions);
    let promise = new Promise ((resolve, reject) =>{
      this.http.put(`https://dev.tuten.cl:443/TutenREST/rest/user/${ email }`, this.body, httpOptions)
      .subscribe( data =>{
        this.info = data;
        this.errorBool = false;
        resolve();
        
      },
      err => {
        this.errores = err;
        this.errorMsg = err.error;
        this.errorBool = true;
        resolve();
      });
    });
    return promise;
  }

}
