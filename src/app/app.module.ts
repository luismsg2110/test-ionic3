import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import {HttpClientModule} from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { SearchPipe } from '../pipes/search/search';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { BookingPage } from '../pages/booking/booking';
import { RestloginProvider } from '../providers/restlogin/restlogin';
import { BookingListProvider } from '../providers/booking-list/booking-list';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    BookingPage,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    BookingPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RestloginProvider,
    BookingListProvider
  ],
exports: [
  SearchPipe
],
})
export class AppModule {}
