import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { BookingListProvider } from '../../providers/booking-list/booking-list';
import { RestloginProvider } from '../../providers/restlogin/restlogin';

/**
 * Generated class for the BookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-booking',
  templateUrl: 'booking.html',
})
export class BookingPage {
  searchString: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private _bs:BookingListProvider, private _rs:RestloginProvider) {
    this._bs.showBook(this._rs.info.email, this._rs.info.sessionTokenBck).then( () =>{
    });
  }

}
