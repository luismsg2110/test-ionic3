import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RestloginProvider } from '../../providers/restlogin/restlogin';
import { BookingPage } from '../booking/booking'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  myForm: FormGroup;
  correo:string;

  constructor(public navCtrl: NavController, public formBuilder: FormBuilder, 
    private _rs:RestloginProvider){

    this.myForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login(){
    this.correo = this.myForm.get('email').value; 
    this._rs.doLogin(<string>this.correo).then( ()=>{
    console.log(this._rs.errorBool);
    if (this._rs.errorBool){

    }else{
      this.navCtrl.push(BookingPage);
    }
    });
  }

}
